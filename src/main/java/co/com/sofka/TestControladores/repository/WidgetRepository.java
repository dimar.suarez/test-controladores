package co.com.sofka.TestControladores.repository;

import co.com.sofka.TestControladores.model.Widget;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WidgetRepository extends MongoRepository<Widget, Long> {
}